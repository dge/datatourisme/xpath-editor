CHANGELOG
===================

Ce fichier est basé sur [Keep a Changelog](http://keepachangelog.com/)
et le projet utilise [Semantic Versioning](http://semver.org/).

## En cours

## [0.1.2] - 2018-11-02

### Modifications
- Browserify
- Correction du calcul d'un contexte relatif

## [0.1.1] - 2017-07-04

### Ajouts

- Navigateur de XPath
- Interface de configuraton des filtres
- Interface mode expert
- Champ input + validateur
- Gestion du contexte