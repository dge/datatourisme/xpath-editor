/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */ 
var xpath = require('js-xpath');
var xquery = require("./xquery");

function parse(expression) {
    if(!expression) {
        return null;
    }
    try {
        var xpathObj = xpath.parse(expression);
        return {
            type: 'xpath',
            expr: xpathObj.toXPath(),
            xpath: xpathObj
        };
    } catch(e) {
        try {
            var out = new xquery.TopDownTreeBuilder();
            var parser = new xquery(expression, out);
            parser.parse_XQuery();
            return {
                type: 'xquery',
                expr: expression
            };
        } catch(e) {
            return null;
        }
    }
}

module.exports = {
    parse: parse,
    xpath: xpath
};