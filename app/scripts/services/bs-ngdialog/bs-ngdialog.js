/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */ 
'use strict';

// service definition
require('angular')
    .module('xpath-editor')
    .factory('bsNgDialog', bsNgDialog);

var $ = require('jquery');

/**
 * @ngInject
 */
function bsNgDialog(ngDialog, $q, $timeout) {

    var service = {
        open: open
    };
    return service;

    /**
     * Open a ngDialog
     */
    function open(params) {
        var dialog;
        var resizer = null;

        params = angular.extend({
            // ngDialog
            showClose: false,
            fullHeight: false,
            fullMaxHeight: false,
            disableAnimation: true,
            overlay: true,
            closeByDocument: true
        }, params);

        // todo : reuse it ?
        var _onOpenCallback = params.onOpenCallback;
        var _preCloseCallback = params.preCloseCallback;

        var modalOptions = angular.extend({}, params.modalOpts, {
            backdrop: params['overlay'] ? (params['closeByDocument'] ? true : 'static') : false,
            keyboard: false
        });

        var onOpenCallback = function() {
            var modal = $(".modal", this);

            if(params.fullHeight || params.fullMaxHeight) {
                resizer = function() {
                    setFullHeight(modal, params.fullHeight);
                };
                modal.on('show.bs.modal', function (e) {
                    resizer();
                    $(window).resize(resizer);
                });
            }

            modal.on('hide.bs.modal', function (e) {
                if(!modal.data("ngdialog.close")) {
                    dialog.close();
                    e.preventDefault();
                }
            });
            modal.modal(modalOptions);
        };

        var preCloseCallback = function() {
            var deferred = $q.defer();
            var modal = $(".modal", this);
            modal.on('hidden.bs.modal', function (e) {
                deferred.resolve();
                if(resizer) {
                    $(window).off("resize", resizer);
                }
            });
            modal.data("ngdialog.close", true);
            modal.modal('hide');
            return deferred.promise;
        };

        params.onOpenCallback = onOpenCallback;
        params.preCloseCallback = preCloseCallback;
        dialog = ngDialog.open(params);
        return dialog;
    }

    function setFullHeight(modal, force) {
        $timeout(function() {

            var $modal          = $(modal);
            var $dialog         = $modal.find('.modal-dialog');
            var $content        = $modal.find('.modal-content');
            var $body           = $content.find('.modal-body');
            var dialogMarginTop = parseInt($dialog.css("marginTop"));
            var windowHeight    = $(window).height();
            var headerHeight    = $content.find('.modal-header').outerHeight() || 0;
            var footerHeight    = $content.find('.modal-footer').outerHeight() || 0;

            //var borderWidth   = this.$content.outerHeight() - this.$content.innerHeight();
            //var dialogMargin  = $(window).width() < 768 ? 20 : 60;
            //var contentHeight = $(window).height() - (dialogMargin + borderWidth);
            //var headerHeight  = this.$element.find('.modal-header').outerHeight() || 0;
            //var footerHeight  = this.$element.find('.modal-footer').outerHeight() || 0;
            //var maxHeight     = contentHeight - (headerHeight + footerHeight);

            var availableHeight     = windowHeight - (dialogMarginTop * 2) - headerHeight - footerHeight;

            $body.css('overflow', 'auto');
            if(force) {
                $body.css('height', availableHeight);
            } else {
                $body.css('max-height', availableHeight);
            }

        }, 200);
    }

}
