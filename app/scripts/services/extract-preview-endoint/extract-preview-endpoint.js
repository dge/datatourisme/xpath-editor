/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */ 
'use strict';

require('angular')
    .module('xpath-editor')
    .provider('extractPreviewEndpoint', extractPreviewEndpoint);

/**
 * @ngInject
 */
function extractPreviewEndpoint() {
    var url = null;

    this.setUrl = function(value) {
        url = value;
    };

    /**
     * @ngInject
     */
    this.$get = function($http) {

        return {
            isAvailable: isAvailable,
            preview: preview
        };

        /**
         *
         */
        function isAvailable() {
            return url !== null;
        }

        /**
         * Query the GraphQL endpoint
         */
        function preview(payload) {
            return $http.post(url, payload).then(function(response) {
                return response.data;
            });
        }
    }
}


