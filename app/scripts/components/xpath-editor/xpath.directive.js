/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */ 
'use strict';

require('angular')
    .module('xpath-editor')
    .directive('xpath', xpathDirective);

var xpath = require('js-xpath');

/**
 * @ngInject
 */
function xpathDirective() {
    return {
        restrict: "A",
        require: "ngModel",
        link: link,
        priority: 1
    };

    /**
     * @ngInject
     */
    function link(scope, elem, attrs, ctrl) {

        ctrl.$formatters.push(function(value) {
            return value ? value.toXPath() : undefined;
        });

        //format text from the user (view to model)
        ctrl.$parsers.push(function(value) {
            if(!value) {
                return null;
            }
            try {
                return xpath.parse(value);
            } catch(e) {
                return undefined;
            }
        });
    }
}
