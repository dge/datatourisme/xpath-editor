/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */ 
'use strict';

require('angular')
  .module('xpath-editor')
  .directive('xpathEditor', xpathEditorDirective);

var parser = require('../../parsers/xexpr');
var xpath = require('js-xpath');

require('./auto-max-height/auto-max-height.directive');
require('./navigator/xpath-navigator.directive');
require('./predicates/xpath-predicates.directive');
require('./expert/xpath-expert.directive');
require('./atomic-chain-editor/atomic-chain-editor');
require('./extract-preview/extract-preview');

/**
 * @ngInject
 */
function xpathEditorDirective(extractPreviewEndpoint) {
    return {
        restrict: 'EA',
        replace: true,
        require: 'ngModel',
        template: require("./xpath-editor.directive.html"),
        scope:{
            fullSchema: "=schema",
            context: "<",
            selector: "@",
            mode: "@"
        },
        link: link
    };

    /**
     * @ngInject
     */
    function link(scope, element, attrs, ngModelCtrl)
    {
        // internal xpath object
        scope.exprObj = {};
        scope.atomic = null;
        scope.ngModelCtrl = ngModelCtrl;

        // selected tab
        scope.selected = null;

        ngModelCtrl.$render = function() {
            if(ngModelCtrl.$modelValue) {
                scope.exprObj = parser.parse(ngModelCtrl.$modelValue.expr);
                scope.atomic = ngModelCtrl.$modelValue.atomic ? ngModelCtrl.$modelValue.atomic : null;
            } else {
                scope.exprObj = scope.atomic = null;
            }
        };

        scope.evaluateXpath = function() {
            scope.exprObj = parser.parse(scope.exprObj.xpath.toXPath());
            scope.$commit();
        };

        scope.evaluateExpr = function() {
            scope.exprObj = parser.parse(scope.exprObj.expr);
            scope.$commit();
        };

        scope.$commit = function() {
            if(scope.exprObj) {
                ngModelCtrl.$setViewValue(angular.extend({}, ngModelCtrl.$modelValue, {
                    expr: scope.exprObj.expr,
                    type: scope.exprObj.type,
                    atomic: scope.atomic && scope.atomic.length ? scope.atomic : undefined
                }));
            } else {
                ngModelCtrl.$setViewValue(undefined);
            }
        }

        // watch fullSchema
        scope.$watch("fullSchema", function(schema) {
            if(schema) {
                scope.schema = processSchema(schema, scope.context);
                if(!scope.selected) {
                    scope.selected = scope.isNavigatorAvailable() ? 'path' : 'expert';
                }
            }
        });

        scope.isNavigatorAvailable = function() {
            return (!scope.exprObj || scope.exprObj.xpath) && !!scope.schema;
        };

        scope.isPredicatesAvailable = function() {
            return scope.isNavigatorAvailable() && scope.getNbPredicates() !== null;
        };

        scope.isPreviewAvailable = function() {
            return ngModelCtrl.$modelValue && ngModelCtrl.$modelValue.expr && extractPreviewEndpoint.isAvailable();
        };

        scope.isAtomicAvailable = function() {
            return scope.selector == "value" && ngModelCtrl.$modelValue && ngModelCtrl.$modelValue.expr;
        };

        scope.getNbPredicates = function() {
            if(scope.isNavigatorAvailable() && scope.exprObj && scope.exprObj.xpath && scope.exprObj.xpath.steps) {
                var count = 0;
                for(var i=0; i<scope.exprObj.xpath.steps.length; i++) {
                    count += scope.exprObj.xpath.steps[i].predicates.length;
                }
                return count;
            }
            return null;
        };
    }
}

/**
 * Process schema with a optional contexts filter
 *
 * @param schema
 * @param contexts
 * @returns {Array}
 */
function processSchema(schema, context) {
    schema.forEach(function(elmt, key) { elmt.id = key; });

    if(!context || !context.length) {
        return schema;
    }

    // extract matched element from fake document
    var elements = [buildDocumentElement(schema)];
    for(var i=0; i<context.length; i++) {
        var nextElements = [];
        for(var j=0; j<elements.length; j++) {
            try {
                var extracted = extractDocumentElements(elements[j], context[i]);
                nextElements = nextElements.concat(extracted);
            } catch(e) {
                // error during parsing : not valid xpath expression, no schema
                return null;
            }
        }
        elements = nextElements;
    }

    // rebuild schema
    schema = [];
    var processElement = function(element, root, parent) {
        schema.push(angular.extend({}, element.schema, {
            parent: parent !== undefined ? element.schema.parent : null,
            xpath: root.schema ? element.schema.xpath.replace(new RegExp("^" + root.schema.xpath + "/","g"), "") : element.schema.xpath
        }));
        for(var i=0; i<element.children.length; i++) {
            processElement(element.children[i], root, element);
        }
    };

    // process elements children
    elements.forEach(function(element) {
        for(var i=0; i<element.children.length; i++) {
            processElement(element.children[i], element);
        }
    });

    return schema;
}

/**
 * Take a xpath object, remove predicates recursively
 *
 * @param xpathObj
 * @returns {*}
 */
function xpathWithoutPredicates(xpathObj) {
    if(xpathObj.steps) {
        return xpath.parse(xpathObj.pathWithoutPredicates());
    } else if(xpathObj.type == "union") {
        xpathObj.left = xpathWithoutPredicates(xpathObj.left);
        xpathObj.right = xpathWithoutPredicates(xpathObj.right);
    }
    return xpathObj;
}

/**
 * @param schema
 * @returns {Element}
 */
function buildDocumentElement(schema) {
    var doc = document.implementation.createDocument(null, "__root__");

    for(var i=0; i<schema.length; i++) {
        var xpath = schema[i].xpath.replace(/:/g, "_ns_").replace(/@/g, "_attr_");
        var split = xpath.split("/");

        // remove first element if empty (root path)
        if(split[0] == "") {
            if(i == 0) {
                doc = document.implementation.createDocument(null, split[1]);
            }
            split.shift();
            split.shift();
        }

        var parent = doc.documentElement;
        for(var j=0; j<split.length; j++) {
            var elmt = null;
            for(var k=0; k < parent.children.length; k++) {
                if(parent.children[k].tagName == split[j]) {
                    elmt = parent.children[k];
                    break;
                }
            }
            if(!elmt) {
                elmt = doc.createElement(split[j]);
                parent.appendChild(elmt);
            }
            parent = elmt;
        }

        parent.schema = schema[i];
    }

    return doc.documentElement;
}


/**
 * @param element
 * @param xpathExpr
 */
function extractDocumentElements(element, xpathExpr) {
    if(!element.ownerDocument) {
        // if element is the document, or the document root return empty array
        return [];
    }

    var ctx = xpathWithoutPredicates(xpath.parse(xpathExpr)).toXPath();
    ctx = ctx.replace(/:/g, "_ns_").replace(/@/g, "_attr_");
    var xpathResult = element.ownerDocument.evaluate( ctx, element, null, XPathResult.ORDERED_NODE_ITERATOR_TYPE, null);

    var elements = [];
    var elmt;
    while (elmt = xpathResult.iterateNext()) {
        if(elmt.ownerDocument || elmt.children[0].tagName != "__root__") {
            // only if not document OR document mode is absolute
            elements.push(elmt);
        }
    }

    return elements;
}