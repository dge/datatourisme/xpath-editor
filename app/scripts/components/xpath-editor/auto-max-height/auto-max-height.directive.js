/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */ 
'use strict';

require('angular')
  .module('xpath-editor')
  .directive('autoMaxHeight', autoMaxHeightDirective);

var $ = require('jquery');

/**
 * @ngInject
 */
function autoMaxHeightDirective($timeout, $window) {
    return {
        restrict: 'EA',
        link: link
    };

    /**
     * @ngInject
     */
    function link(scope, elem, attrs) {
        $timeout(fixGridMaxHeight, 200);
        angular.element($window).bind('resize', fixGridMaxHeight);

        scope.$on('$destroy', function () {
            angular.element($window).off('resize', fixGridMaxHeight);
        });
        //var test = $('<div style="position: absolute; left: 50%; z-index:999999; width: 20px; height: 20px; background: red">qsd</div>');
        //test.appendTo($("body"));

        /**
         * fixGridMaxHeight
         */
        function fixGridMaxHeight() {

            var dialog = $(elem).parents('.modal-dialog');
            var elmtWrapper = $(elem);
            var windowHeight = $window.innerHeight;
            var relTop = elmtWrapper.offset().top - dialog.offset().top;

            // calculate maxheight
            var footerHeight = $(".modal-footer", dialog).outerHeight();
            var marginBottom = parseInt(dialog.css("margin-bottom"));
            var posBottom = windowHeight - footerHeight - marginBottom;
            var maxHeight = posBottom - relTop - parseInt(dialog.css("margin-top"));

            // calculate minheight
            var tabContent = $(".tab-content", dialog);
            relTop = elmtWrapper.offset().top - tabContent.offset().top;
            var minHeight = parseInt($(".tab-content", dialog).css("min-height")) + - relTop;

            if(maxHeight < minHeight) {
                maxHeight = minHeight;
            }
            elmtWrapper.css('max-height', maxHeight + 'px');
        }
    }
}
