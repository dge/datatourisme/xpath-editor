/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */ 
'use strict';

var angular = require('angular');

angular
    .module('xpath-editor')
    .component('extractPreview', {
        template: require("./extract-preview.html"),
        controller: controller,
        bindings: {
            "expr": "<",
            "selector": "@",
            "context": "<"
        }
    });

window.CodeMirror = require("codemirror");
require("codemirror/mode/xml/xml");

/**
 * @ngInject
 */
function controller(extractPreviewEndpoint) {
    var $ctrl = this;
    this.data = null;
    this.error = null;
    this.loading = true;

    this.$onChanges = function() {
        this.$init();
    };

    this.$init = function() {
        this.keys = Object.keys;
        var payload = {
            selector: this.selector,
            expr: this.expr.expr,
            context: this.context,
            atomic: this.expr.atomic
        };
        this.loading = true;
        this.error = null;

        this.codemirrorOpts = {
            lineWrapping : true,
            lineNumbers: false,
            readOnly: 'nocursor',
            mode: 'xml'
        };

        extractPreviewEndpoint.preview(payload).then(function(data) {
            $ctrl.data = data;
            $ctrl.loading = false;
        }, function(response) {
            $ctrl.data = null;
            $ctrl.error = response.data && response.data.error ? response.data.error.message :
                (response.status > 0 ? "Le serveur a rencontré une erreur." : "Impossible de contacter le serveur.");
            $ctrl.loading = false;
        });
    };
}
