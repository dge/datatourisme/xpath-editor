/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */ 
'use strict';

var angular = require('angular');

angular
    .module('xpath-editor')
    .component('atomicChainEditor', {
        template: require("./atomic-chain-editor.html"),
        controller: controller,
        require: {
            ngModelCtrl: 'ngModel'
        },
        bindings: {
            "sample": "@"
        }
    });

require('./operations/**/*.js', {mode: 'expand'});

var ngDialogOpts = {
    appendTo: '.xpath-editor',
    className: 'offcanvas offcanvas-atomic-chain',
    bodyClassName: 'offcanvas-open',
    plain: true,
    trapFocus: false,
    showClose: false,
    controllerAs: "$ctrl"
};

/**
 * @ngInject
 */
function controller($scope, ngDialog, $timeout, $injector, atomicOperationRegistry) {
    var $ctrl = this;
    this.operations = [];
    this.sampleResult = null;

    this.$onInit = function() {
        this.ngModelCtrl.$render = this.$init;
    };

    this.$init = function() {
        $ctrl.operations = $ctrl.ngModelCtrl.$modelValue ? $ctrl.ngModelCtrl.$modelValue : [];
    };

    this.$commit = function() {
        $ctrl.ngModelCtrl.$setViewValue($ctrl.operations.length > 0 ? angular.copy($ctrl.operations) : undefined);
        //@todo : update sample
        //updateExamplePreview();
    };

    /**
     * Return operation detail by definition
     *
     * @param def
     * @returns {*|void}
     */
    this.getOperationByDef = function(def) {
        for(var name in def) {
            return angular.extend({}, atomicOperationRegistry[name], {
                idx:  this.operations.indexOf(def),
                name: name,
                args: def[name],
                def: def,
                configurable: isConfigurable(name)
            });
        }
    };

    /**
     * Add an operation
     */
    this.addOperation = function() {
        var dialog = ngDialog.open(angular.extend({}, ngDialogOpts, {
            template: require("./atomic-chain-operation-list.html"),
            controller: operationListController,
            controllerAs: "$ctrl"
        }));

        dialog.closePromise.then(function (dialog) {
            if(validDialogResponse(dialog)) {
                var name = dialog.value;
                var def = {};
                def[name] = [];
                if(isConfigurable(name)) {
                    var dialog = operationDialog($ctrl.getOperationByDef(def));
                    dialog.closePromise.then(function (dialog) {
                        if(validDialogResponse(dialog)) {
                            def[name] = dialog.value;
                            $ctrl.operations.push(def);
                            $ctrl.$commit();
                        }
                    });
                } else {
                    $ctrl.operations.push(def);
                    $ctrl.$commit();
                }
            }
        });
    };

    /**
     * Edit an operation
     *
     * @param operation
     */
    this.editOperation = function(operation) {
        var dialog = operationDialog(operation);
        dialog.closePromise.then(function (dialog) {
            if(validDialogResponse(dialog)) {
                if(dialog.value == "delete") {
                    // HOW ????
                } else {
                    operation.args = dialog.value;
                    operation.def[operation.name] = dialog.value; // update model by reference
                }
            }
        });
    };

    /**
     * Delete operation by it position
     *
     * @param idx
     */
    this.deleteOperation = function(operation) {
        if(confirm("Êtes-vous certain de vouloir supprimer ce traitement ?")) {
            this.operations.splice(operation.idx, 1);
            this.$commit();
        }
    };

    /**
     * Delete operation by it position
     *
     * @param operation
     */
    this.upOperation = function(operation) {
        if(operation.idx < this.getModelValue().length - 1) {
            var old = this.operations[operation.idx + 1];
            this.operations[operation.idx + 1] = this.operations[operation.idx];
            this.operations[operation.idx] = old;
            this.$commit();
        }
    };

    /**
     * Delete operation by it position
     *
     * @param operation
     */
    this.downOperation = function(operation) {
        if(operation.idx > 0) {
            var old = this.operations[operation.idx - 1];
            this.operations[operation.idx - 1] = this.operations[operation.idx];
            this.operations[operation.idx] = old;
            this.$commit();
        }
    };

    /**
     * is configurable operation ?
     *
     * @param name
     * @returns {*}
     */
    function isConfigurable(name) {
        return $injector.has('atomicOperation' + name.charAt(0).toUpperCase() + name.slice(1) + 'Directive');
    }

    /**
     * updateExamplePreview
     */
    function updateExamplePreview() {
        /*if(!$ctrl.getModelValue().length) {
            $ctrl.sampleResult = null;
            return;
        }
        if($ctrl.sample) {
            apiEndpoints.atomic.preview([$ctrl.sample], $ctrl.getModelValue()).then(function(data) {
                $ctrl.sampleResult = data;
            });
        }*/
    }

    /**
     * Predicate dialog
     *
     * @param operation
     * @returns {*}
     */
    function operationDialog(operation) {
        return ngDialog.open(angular.extend({}, ngDialogOpts, {
            template: require("./atomic-chain-operation-edit.html"),
            resolve: {
                operation: function() {
                    return angular.copy(operation);
                }
            },
            controller: operationEditController
        }));
    }
}

/**
 * @param response
 * @returns {*|boolean}
 */
function validDialogResponse(response) {
    return (response.value && response.value.indexOf('$') == -1);
}

/**
 * @ngInject
 */
function operationListController(atomicOperationRegistry) {
    var groups = {};
    for(var name in atomicOperationRegistry) {
        var op = atomicOperationRegistry[name];
        groups[op.type] = groups[op.type] ? groups[op.type] : [];
        groups[op.type].push(angular.extend({}, op, {
            name: name
        }));
    }
    this.groups = groups;
}

/**
 * @ngInject
 */
function operationEditController($scope, operation, $timeout) {
    this.operation = operation;
    this.submit = function() {
        if(this.form.$valid) {
            $scope.closeThisDialog(operation.args);
        }
    };
    this.delete = function() {
        $scope.closeThisDialog("delete");
    }
}