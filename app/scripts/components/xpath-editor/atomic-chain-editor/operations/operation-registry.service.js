/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */ 
'use strict';

var angular = require('angular');

angular
    .module('xpath-editor')
    .constant('atomicOperationRegistry', atomicOperationRegistry());

/**
 * @ngInject
 */
function atomicOperationRegistry() {
    return {
        // expand
        "split": {
            "label": "Séparateur",
            "description": "Découpe une valeur en utilisant un séparateur pour constituer un ensemble",
            "type": "expand"
        },
        // reduce
        "first": {
            "label": "Premier",
            "description": "Réduit un ensemble à son premier élément",
            "type": "reduce"
        },
        "last": {
            "label": "Dernier",
            "description": "Réduit un ensemble à son dernier élément",
            "type": "reduce"
        },
        "index": {
            "label": "Index",
            "description": "Réduit un ensemble à son nème élément",
            "type": "reduce"
        },
        "join": {
            "label": "Concaténation",
            "description": "Concatène les éléments d'un ensemble en utilisant un séparateur",
            "type": "reduce"
        },
        "count": {
            "label": "Comptabilise",
            "description": "Retourne le nombre d'éléments de l'ensemble",
            "type": "reduce"
        },
        // map
        "replace": {
            "label": "Remplace",
            "description": "Remplace dans le texte",
            "type": "map"
        },
        "default": {
            "label": "Valeur par défaut",
            "description": "Défini une valeur par défaut si l'ensemble est vide",
            "type": "map"
        },
        "distinct": {
            "label": "Unique",
            "description": "Retire les doublons d'un ensemble",
            "type": "map"
        },
        //"map": {
        //    "label": "Correspondances",
        //    "description": "Utilise une table de correspondance pour le remplacement de valeurs",
        //    "type": "map"
        //},
        "lowercase": {
            "label": "Minuscules",
            "description": "Transforme le texte en minuscule",
            "type": "map"
        },
        "regex": {
            "label": "Expression régulière",
            "description": "Extrait une valeur à partir d'une expression régulière",
            "type": "map"
        },
        "concat": {
            "label": "Mise à plat",
            "description": "Regroupe le résultat des traitements précédents dans un seul ensemble",
            "type": "map"
        },
        "html2text": {
            "label": "Nettoyage HTML",
            "description": "Nettoie le texte du language HTML tout en préservant les retours à la ligne",
            "type": "map"
        }
    };
}