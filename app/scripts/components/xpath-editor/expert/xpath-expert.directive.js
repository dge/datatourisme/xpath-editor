/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */ 
'use strict';

require('angular')
  .module('xpath-editor')
  .directive('xpathExpert', xpathExpertDirective);

var parser = require('../../../parsers/xexpr');
window.CodeMirror = require("codemirror");
require("codemirror/mode/xquery/xquery");
require("codemirror/addon/display/placeholder");

/**
 * @ngInject
 */
function xpathExpertDirective($timeout) {
    return {
        restrict: 'EA',
        replace: true,
        require: 'ngModel',
        template: require("./xpath-expert.directive.html"),
        controller: controller,
        scope: {
            mode: "@"
        },
        link: link
    };

    /**
     * @ngInject
     */
    function link(scope, elem, attrs, ngModelCtrl) {

        // watch exprObj
        scope.$watch(function() {
            return ngModelCtrl.$modelValue
        }, function(_new, _old) {
            if(!scope.proxyObj || scope.proxyObj.expr != _new) {
                scope.proxyObj = parser.parse(_new);
                $timeout(function() { scope.refresh = true; }, 200);
            }
        });

        // watch proxyObj
        scope.$watch("proxyObj", function(_new, _old) {
            if(_new !== undefined) {
                ngModelCtrl.$setViewValue(_new ? _new.expr : null);
            }
        });

    }

    /**
     * @ngInject
     */
    function controller($scope, $timeout) {
        $scope.codemirrorOpts = {
            lineWrapping : true,
            mode: 'xquery',
            placeholder: 'Commencez à saisir une expression...'
        };
    }
}