/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */ 
'use strict';

require('angular')
  .module('xpath-editor')
  .directive('xpathNavigator', xpathNavigatorDirective);

var $ = require('jquery');
var xpath = require('js-xpath');
require('jstree');
require('jstreegrid/jstreegrid');

/**
 * @ngInject
 */
function xpathNavigatorDirective($timeout, $window) {
    return {
        restrict: 'EA',
        require: 'ngModel',
        scope: {
            schema: "=",
            selector: "@"
        },
        template: require("./xpath-navigator.directive.html"),
        link: link
    };

    /**
     * @ngInject
     */
    function link(scope, elem, attrs, ngModelCtrl) {
        var jstree, data;
        var selector = scope.selector ? scope.selector : "value";

        scope.search = null;
        scope.xpath = null;

        scope.$watch("schema", function(schema) {
            if(schema) {
                data = processSchemaData(schema);
                $init();
            }
        });

        /**
         * ngModel $render
         */
        ngModelCtrl.$render = function() {
            scope.xpath = ngModelCtrl.$modelValue ? ngModelCtrl.$modelValue.toXPath() : null;
        };

        /**
         * commit : parse xpath expression
         */
        function $commit(expr) {
            ngModelCtrl.$setViewValue(xpath.parse(expr));
        }

        /**
         * init
         */
        function $init() {
            jstree = $(".jstree", elem).jstree({
                plugins: ["core", "state", "search", "grid"],
                "core" : {
                    //"animation" : 0,
                    worker: false,
                    multiple: false,
                    data : dataFilter(data, selector)
                },
                grid: {
                    columns: [
                        {header: "Element XML"},
                        {value: "card", header: "Card."},
                        {value: "sample", header: "Exemple de valeur", cellClass: "text-muted"},
                    ],
                    width: "100%"
                },
                search: {
                    show_only_matches: true
                }
            });

            var timer;
            jstree.on("render_cell.jstree-grid", function(event, data) {
                $timeout.cancel( timer );
                timer = $timeout(function() {
                    $(".jstree-container-ul li").each(function(i) {
                        var disabled = $("> a", this).hasClass('jstree-disabled');
                        var method = disabled ? "addClass" : "removeClass";
                        $(".jstree-grid-column:not(.jstree-grid-column-0)").each(function() {
                            $("> .jstree-grid-cell", $(this)).eq(i)[method]('jstree-disabled');
                        });
                    });
                });
            });

            jstree.on('state_ready.jstree', function (e, data) {
                $timeout(function() {
                    selectTreeNode(scope.xpath);
                    scope.$watch("xpath", selectTreeNode);
                    jstree.on('changed.jstree', onJsTreeChange);
                });
            });

            jstree.on('after_open.jstree', function (e, data) {
                $timeout(function() {
                    if(!data.node.a_attr.id) {
                        return;
                    }
                    var height = $("#" + data.node.a_attr.id).parent("li").outerHeight();
                    var top = $("#" + data.node.a_attr.id).parent("li").position().top;
                    var scrollTop = $(".jstree-grid-wrapper").scrollTop();
                    var wrapperHeight = $(".jstree-grid-wrapper").outerHeight() - $(".jstree-grid-header").outerHeight();
                    if(top + height - scrollTop  > wrapperHeight) {
                        var scroll = (top + height) - wrapperHeight;
                        if(scroll > top) {
                            scroll = top;
                        }
                        $('.jstree-grid-wrapper').animate({
                            scrollTop: scroll
                        }, 200);
                    }
                });
            });

            // handle fixGridMaxHeight
            $timeout(fixGridMaxHeight, 200);
            angular.element($window).bind('resize', fixGridMaxHeight);
            // ---
        }

        scope.$watch("search", function(_new, _old) {
            if(_new !== _old) {
                $(".jstree", elem).jstree(true).search(_new ? _new : "");
            }
        });

        /**
         * @param e
         * @param data
         */
        function onJsTreeChange(e, data) {
            var node = data.instance.get_node(data.selected[0]);
            if(node && node.original.xpath) {
                var xpathObj;
                if(scope.xpath) {
                    var xpathObj = xpath.parse(scope.xpath);
                    if(xpathObj.steps) {
                        xpathObj = mergeXPathExpressions(scope.xpath, node.original.xpath);
                    } else {
                        xpathObj = xpath.parse(node.original.xpath);
                    }

                } else {
                    xpathObj = xpath.parse(node.original.xpath);
                }
                // avoid inprogress error
                if(scope.xpath != xpathObj.toXPath()) {
                    scope.$evalAsync(function() {
                        $commit(xpathObj.toXPath());
                    });
                }
            }
        }

        /**
         * @param _xpath
         */
        function selectTreeNode(_xpath) {
            $.jstree.reference(jstree).deselect_all(true);
            try {
                // test XPath expression
                var xpathObj = xpath.parse(_xpath);
            } catch (e) {
                return;
            }

            var xpathExprs = getRecursiveXPathExpressions(xpathObj);
            var selected = [];
            for(var i=0; i<xpathExprs.length; i++) {
                _xpath = xpathExprs[i].pathWithoutPredicates();
                for(var j=0; j<data.length; j++) {
                    if(data[j].xpath == _xpath) {
                        selected.push(data[j].id);
                        break;
                    }
                }
            }

            $.jstree.reference(jstree).select_node(selected, true);
        }

        /**
         * @param xpathObj
         * @returns {Array}
         */
        function getRecursiveXPathExpressions(xpathObj) {
            var exprs = [];
            if(xpathObj.steps) {
                exprs.push(xpathObj);
            } else if(xpathObj.type == "union") {
                exprs = exprs.concat(
                    getRecursiveXPathExpressions(xpathObj.left),
                    getRecursiveXPathExpressions(xpathObj.right)
                );
            }
            return exprs;
        }


        /**
         * fixGridMaxHeight
         */
        function fixGridMaxHeight() {
            var $wrapper = $(".jstree-grid-wrapper", elem);
            $wrapper.css("max-height", $wrapper.parent().css("max-height"));
        }
    }

    /**
     *
     * @param data
     */
    function dataFilter(data, selector) {
        var nbRoot = data.filter(function(d) { return d.parent == '#'}).length;
        for(var i = 0; i<data.length; i++) {
            if(data[i].type == 'attribute') {
                data[i].icon = 'fa fa-tag';
            } else {
                data[i].icon = 'fa fa-code';
            }

            var card = "";
            if(data[i].minOccurs == 0 && data[i].maxOccurs == 1)  {
                card = '<span class="label label-warning" title="Cet élément peut ne pas être présent. S\'il est présent, il n\'est présent qu\'une fois.">';
            } else if(data[i].minOccurs == 0 && data[i].maxOccurs > 1)  {
                card = '<span class="label label-warning" title="Cet élément peut ne pas être présent ou apparaître plusieurs fois.">';
            } else if(data[i].minOccurs == 1 && data[i].maxOccurs > 1)  {
                card = '<span class="label label-default" title="Cet élément est systématiquement présent. Il peut apparaître plusieurs fois.">';
            } else if(data[i].minOccurs > 1 && data[i].maxOccurs > 1)  {
                card = '<span class="label label-default" title="Cet élément est systématiquement présent et apparaît plusieurs fois.">';
            } else {
                card = '<span class="label label-default" title="Cet élément est systématiquement présent.">';
            }
            card += data[i].minOccurs+'..'+data[i].maxOccurs+'</span>';


            data[i].data = {
                card: card,
                sample: data[i].value ? data[i].value : "-"
            };

            if(data[i].empty) {
                //console.log(data[i]);
                // @todo : find the empty bu in worker
                //    data.splice(i, 1);
                //    i--;
            }

            data[i].state = {};
            if(data[i].parent == '#' && nbRoot <= 5) {
                data[i].state.opened = true;
            }

            if(selector) {
                var disabled = !(
                    (data[i].type == 'attribute' && selector == "value") ||
                    (data[i].type == 'element' && data[i].value && selector == "value") ||
                    (data[i].type == 'element' && hasChildren(data[i].id) && selector == "element")
                );

                data[i].state.disabled = disabled;
            }


            data[i].text = data[i].name;
        }

        function hasChildren(id) {
            for(var i=0; i<data.length; i++) {
                if(data[i].parent == id) {
                    return true;
                }
            }
            return false;
        }

        return data;
    }

    /**
     * @param xpath1
     * @param xpath2
     * @returns {*}
     */
    function mergeXPathExpressions(xpath1, xpath2) {
        var xpathObj1 = xpath.parse(xpath1);
        var xpathObj2 = xpath.parse(xpath2);

        var steps = xpathObj2.steps;
        for(var i=0; i<steps.length; i++) {
            if(!xpathObj1.steps[i]
                || steps[i].predicates.length > 0
                || (steps[i].mainXPath() != xpathObj1.steps[i].mainXPath())) {
                break;
            }
            steps[i] = xpathObj1.steps[i];
        }

        xpathObj2.steps = steps;
        return xpathObj2;
    }


    /**
     * Process schema to jstree data
     *
     * @param schema
     * @returns {Array}
     */
    function processSchemaData(schema) {
        var data = [];
        for(var i=0; i<schema.length; i++) {
            data[i] = angular.extend({}, schema[i], {
                id: schema[i].id !== undefined ? schema[i].id : i,
                parent: schema[i].parent == null ? '#' : schema[i].parent
            });
        }
        data = data.sort(function(a,b) {return (a.xpath > b.xpath) ? 1 : ((b.xpath > a.xpath) ? -1 : 0);} );
        return data;
    }
}
