/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */ 
'use strict';

// module definition
require('angular')
    .module('xpath-editor')
    .controller('XpathPredicateDialogController', XpathPredicateDialogController);

var xpath = require('js-xpath');
require('./xpath-autocomplete.directive');

/**
 * @ngInject
 */
function XpathPredicateDialogController($scope, predicate, partialXpath, schema) {
    $scope.predicate = predicate;

    var _xpath = partialXpath.pathWithoutPredicates();
    var paths = [];
    for(var i=0; i<schema.length; i++) {
        if(schema[i].xpath.indexOf(_xpath + "/") == 0) {
            paths.push({
                path: schema[i].xpath.substr(_xpath.length + 1),
                example: schema[i]['text'] ? schema[i]['text'] : schema[i]['value']
            })
        }
    }
    $scope.paths = paths;

    $scope.data = {
        type: 'eq'
    };

    if(predicate) {
        if(predicate instanceof xpath.yy.xpathmodels.XPathEqExpr) {
            $scope.data.type = "eq";
            $scope.data.xpathObj = predicate.left;
            $scope.data.value = predicate.right.value;
        } else {
            $scope.data.type = "path";
            $scope.data.xpathObj = predicate;
        }
    }

    /***
     *
     */
    $scope.validate = function() {
        if($scope.form.$valid) {
            var expr;
            if($scope.data.value) {
                expr = xpath.parse($scope.data.xpathObj.toXPath() + " = " + JSON.stringify($scope.data.value));
            } else {
                expr = $scope.data.xpathObj;
            }
            $scope.closeThisDialog(expr);
        }
    };

    /**
     *
     */
    $scope.delete = function() {
        $scope.closeThisDialog("delete");
    };

}

