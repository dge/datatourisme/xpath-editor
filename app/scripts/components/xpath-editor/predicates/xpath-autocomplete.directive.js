/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */ 
'use strict';

require('angular')
  .module('xpath-editor')
  .directive('xpathAutocomplete', xpathAutocompleteDirective);

var $ = require('jquery');
require('typeahead.js');

/**
 * @ngInject
 */
function xpathAutocompleteDirective($timeout) {
    return {
        restrict: "A",
        require: "ngModel",
        scope: {
          paths: "="
        },
        link: link
    };

    /**
     * @ngInject
     */
    function link(scope, elem, attrs, ctrl) {
        var $elem = $(elem);

        // watch model to update typeahea
        scope.$watch(function () {
            return ctrl.$viewValue;
        }, function (_new) {
            $elem.typeahead('val', _new);
        });

        scope.$watch("paths", function(paths) {
            init(paths);
        });

        function init(paths) {
            // init typeahead
            $elem.typeahead({
                    hint: false,
                    highlight: false,
                    minLength: 0
                },
                {
                    limit: 10,
                    name: 'paths',
                    displayKey: 'path',
                    source: suggestionsMatcher(paths),
                    templates: {
                        suggestion: function(data){
                            return '<div>' +
                                (data.example ? '<small class="text-muted">' + data.example +'</small>' : '') +
                                //(data.type ? '<span class="label label-default">' + data.type +'</span> ' : '') +
                                (data.parent ? '.. /' : '') + '<strong>' + data.key + '</strong><br>' +
                                '</div>';
                        }
                    }
                }).bind('typeahead:selected', function(obj, datum, name) {
                // set ng-model value
                ctrl.$setViewValue(datum.path);
                $timeout(function() {
                    // force re-open
                    $elem.data().ttTypeahead.input.trigger('queryChanged', datum.path);
                });
            });
        }
    }
}

/**
 * Suggestions matcher
 * @param _paths
 * @returns {Function}
 */
function suggestionsMatcher(_paths) {

        // prepare paths suggestions from _paths
        var paths = {};
        var pathObj, pathArr, parent, path;
        var i, j, k;

        for(i=0; i<_paths.length; i++) {
            pathObj = _paths[i];
            if(typeof pathObj == "string") {
                pathObj = {path: pathObj};
            }
            pathArr = pathObj.path.split('/');
            pathLoop: for(j=0 ;j < pathArr.length; j++) {
                parent = j ? pathArr.slice(0, j).join("/") : "";
                path = (parent ? parent + '/' : '') + pathArr.slice(j, j+1).join("/");
                paths[parent] = paths[parent] ? paths[parent] : [];

                // if already exist, go next
                for(k=0; k<paths[parent].length; k++) {
                    if(paths[parent][k].path == path) {
                        continue pathLoop;
                    }
                }

                var obj = {
                    path: path,
                    parent: parent + (j ? '/' : ''),
                    key: pathArr.slice(j, j+1)[0]
                };

                if(path == pathObj.path) {
                    obj = $.extend({}, obj, pathObj);
                }

                if(!obj.type) {
                    if(obj.key.indexOf('@') == 0) {
                        obj.type = 'attr';
                    } else if(obj.key == 'text()') {
                        obj.type = 'txt';
                    } else {
                        obj.type = 'elem';
                    }
                }

                // add to paths
                paths[parent].push(obj);
            }
        }

        // return compaing function
        return function(q, cb) {
            var path = q.split('/');
            path.splice(-1,1)[0];
            path = path.join('/');

            var matches = [];
            var substrRegex = new RegExp(q, 'i');

            if(paths[path]) {
                $.each(paths[path], function(i, path) {
                    if (substrRegex.test(path.path)) {
                        matches.push(path);
                    }
                });
            }

            // if the current query has children, concat them
            if(q && paths[q]) {
                matches = matches.concat(paths[q]);
            }

            // remove current value from matches
            for(var i=0; i<matches.length; i++) {
                if(matches[i].path == q) {
                    matches.splice(i,1);
                    i--;
                }
            }

            cb(matches);
        };
    }
