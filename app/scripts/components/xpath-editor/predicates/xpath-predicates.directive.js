/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */ 
'use strict';

require('angular')
  .module('xpath-editor')
  .directive('xpathPredicates', xpathPredicatesDirective);

var xpath = require('js-xpath');
require('./xpath-predicates-dialog.controller');

/**
 * @ngInject
 */
function xpathPredicatesDirective(ngDialog) {
    return {
        restrict: 'EA',
        require: 'ngModel',
        scope: {
            schema: "="
        },
        template: require("./xpath-predicates.directive.html"),
        link: link
    };

    /**
     * @ngInject
     */
    function link(scope, elem, attrs, ngModelCtrl) {
        scope.xpathObj = null;

        ngModelCtrl.$render = function() {
            scope.xpathObj = ngModelCtrl.$modelValue;
        };

        function $commit() {
            ngModelCtrl.$setViewValue(angular.copy(scope.xpathObj));
        }

        /**
         * edit predicate dialog
         *
         * @param predicates
         * @param key
         */
        scope.edit = function(step, key) {
            var dialog = predicateDialog(step, key);
            dialog.closePromise.then(function (data) {
                // closing
                if(data.value.indexOf && data.value.indexOf('$') > -1) return;
                // delete
                if(data.value == "delete") {
                    step.predicates.splice(key, 1);
                    $commit();
                    return;
                }
                // submit
                step.predicates[key] = data.value;
                $commit();
            });
        };


        /**
         * add predicate dialog
         *
         * @param predicates
         */
        scope.add = function(step) {
            var dialog = predicateDialog(step);
            dialog.closePromise.then(function (data) {
                // closing
                if(data.value.indexOf && data.value.indexOf('$') > -1) return;
                // submit
                step.predicates.push(data.value);
                $commit();
            });
        };

        /**
         * Predicate dialog
         *
         * @param predicate
         * @returns {*}
         */
        function predicateDialog(step, key) {

            var stepIndex = scope.xpathObj.steps.indexOf(step);
            return ngDialog.open({
                appendTo: '.xpath-editor',
                template: require('./xpath-predicates-dialog.html'),
                plain: true,
                className: 'offcanvas offcanvas-xpath-predicates',
                bodyClassName: 'offcanvas-open',
                trapFocus: false,
                showClose: false,
                resolve: {
                    partialXpath: function() { return getPartialXpath(scope.xpathObj, stepIndex) },
                    predicate: function() { return key != null ? step.predicates[key] : null; },
                    schema: function() { return scope.schema; }
                },
                controller: "XpathPredicateDialogController"
            });
        }
    }
}

/**
 *
 * @param xpathObj
 * @param stepIndex
 * @returns {makeXPathModels.XPathPathExpr}
 */
function getPartialXpath(xpathObj, stepIndex) {
    return new xpath.yy.xpathmodels.XPathPathExpr({
        initial_context: xpathObj.initial_context,
        steps: xpathObj.steps.slice(0, stepIndex+1)
    });
}