/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */ 
'use strict';

window.jQuery = window.$ = require('jquery');
var angular = require('angular');

// vendors
require('bootstrap');
require('angular-ui-codemirror');

// module definition
angular.module('xpath-editor', [
    require('ng-dialog'),
    'ui.codemirror'
]);

require("./**/*.html", {mode: 'expand'});
require('./services/*/*.js', {mode: 'expand'});
require('./components/*/*.js', {mode: 'expand'});